import org.jetbrains.kotlin.gradle.tasks.KotlinCompile


group = "de.bstachmann"
version = "1.0.0-SNAPSHOT"


plugins {
    kotlin("jvm") version "1.3.20"
}


allprojects {

    repositories {
        jcenter()
    }

    apply(plugin = "org.jetbrains.kotlin.jvm")

    tasks.withType<Test> {
        useJUnitPlatform()
    }

    tasks.withType<KotlinCompile> {
        kotlinOptions.jvmTarget = "1.8"
    }

    dependencies {
        api(kotlin("stdlib-jdk8"))

        implementation("junit:junit:4.4")
        implementation("org.assertj:assertj-core:3.11.1")

    }

}
dependencies {
    implementation(kotlin("stdlib-jdk8"))
}
repositories {
    mavenCentral()
}
val compileKotlin: KotlinCompile by tasks
compileKotlin.kotlinOptions {
    jvmTarget = "1.8"
}
val compileTestKotlin: KotlinCompile by tasks
compileTestKotlin.kotlinOptions {
    jvmTarget = "1.8"
}