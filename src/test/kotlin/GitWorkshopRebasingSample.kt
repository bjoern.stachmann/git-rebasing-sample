import org.assertj.core.api.Assertions.assertThat
import org.junit.Test

/**
 * Beispiel für Git-Workshrop!
 *
 */
class GitWorkshopRebasingSample {

    @Test
    fun gitWorkshopBeispiel() {


        data class Rechteck(val breite: Double, val hoehe: Double)


        var r = Rechteck(3.0, 4.0)

        assertThat(r.toString())
                .isEqualTo("Rechteck(breite=3.0, hoehe=4.0)")

        assertThat(r)
                .isEqualTo(Rechteck(3.0, 4.0))
                .isNotEqualTo(Rechteck(11.0, 4.0))
                .isNotEqualTo(Rechteck(3.0, 11.0))
                .`as`("Gleichheit durch Feldvergleiche")

    }

}